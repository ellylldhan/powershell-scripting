Network_Pi.ps1
---------------------
Remplace le script Script Scan_Réseau.ps1
Plusieurs outils pour petites investigations.
La vitesse du scan a été amélioré passant de 40 min à 2 min 30 environ pour un réseau en /24.



Script ADTool1.ps1
------------------
Procédure pour script ADTool1.ps1 --> Procédure Script ADTool1.docx
Il y a quelques infos également en commentaire dans le script.

Ce script a été testé de mon coté et utilisé en environnement de travail mais je sais que vous savez qu'il faut bien sur 
jeter un oeil sur le contenu de tout nouveau script et le tester sur une VM test :)


Script PCTool1.ps1
------------------
Certains résultats ne s'afficheront que si le script est lancé en administrateur, notamment pour BitLocker.
Voir également le script pour avoir quelques commentaires sur celui-ci.

Pour la désinstallation de logiciel, il peut y avoir des comportements différents, sur mon PC personnel, 
par exemple pour VLC la fenêtre de désinstallation s'affiche et il faut appuyer sur OK.

Sur les PC de mon organisation, il n' y a aucune fenêtre visible et cela désinstalle directement le soft en mode silencieux.



Script Calcul_Réseaux.ps1
-------------------------
Quelques infos en commentaire dans le script

1 - prend l'adresse IP du PC sur lequel il est lancé et calcul l'adresse IP du réseau, la première et la dernière adresse IP machine et l'adresse de broadcast.
2 - idem mais on rentre l'adresse IP et le masque sous-réseau que l'on souhaite
3 - Convertisseur d'adresse IP Décimal en Binaire
4 - Convertisseur d'adresse IP Binaire en Décimal
