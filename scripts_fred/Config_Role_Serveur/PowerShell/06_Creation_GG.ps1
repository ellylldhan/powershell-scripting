﻿    If (-NOT ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
{   
$arguments = "& '" + $myinvocation.mycommand.definition + "'"
Start-Process powershell -Verb runAs -ArgumentList $arguments
Break
}

Import-Module ActiveDirectory


Do {
#Création des groupes globaux

Write-Host "###### Création des groupes globaux ######" -ForegroundColor Green
Write-Host ""

$OU_GG_Path = (Get-ADOrganizationalUnit -Filter "Name -eq 'GG'").DistinguishedName

$GroupeGlobal = Read-Host "Indiquer le nom du groupe, laisser vide pour mettre fin au script"

if ($GroupeGlobal -like $null){
exit
} 

New-ADGroup -Name $GroupeGlobal -Path $OU_GG_Path -GroupScope global


Write-Host ""
Write-Host "###### Groupes de partage DL actuels ######" -ForegroundColor Green


Get-ADGroup -Filter * | where-object Name -like "Partage*" | ft Name
write-host ""

# Ajout du groupe dans un groupe de partage DL

$Groupe_DL = Read-Host "Indiquer le groupe de partage dans lequel ajouter le groupe précédemment créé, ne rien indiquer si non necessaire"
Write-Host ""

try{

Add-AdGroupMember -Identity $Groupe_DL -Members $GroupeGlobal
write-host "Le groupe $GroupeGlobal a été ajouté en tant que membre du groupe $Groupe_DL"
Write-Host ""
pause
Cls
}

catch {
if ($Groupe_DL -like $null){
Write-Host ""
write-host "Pas de groupe indiqué, le script va rebooter"
Write-Host ""
pause
Cls
}

else {
Write-Error "$Error[0]"
pause
Cls
}

}


}

until ($GroupeGlobal -like $null)