﻿    If (-NOT ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
{   
$arguments = "& '" + $myinvocation.mycommand.definition + "'"
Start-Process powershell -Verb runAs -ArgumentList $arguments
Break
}


    $NomDUDomaine = (gwmi WIN32_ComputerSystem).Domain
    $NomDomaineSplit = $NomDUDomaine.Split(".")

    $a = $NomDomaineSplit[0]
    $b = $NomDomaineSplit[1]


$CSVFile = "C:\Users\Administrateur\Desktop\userAD.csv"
$CSVData = Import-CSV -Path $CSVFile -Delimiter "," -Encoding UTF8

Foreach($Utilisateur in $CSVData){

    $UtilisateurPrenom = $Utilisateur.Prenom
    $UtilisateurNom = $Utilisateur.Nom
    $UtilisateurFonction = $Utilisateur.fonction
    $UtilisateurAgence = $Utilisateur.agence
    $UtilisateurService = $Utilisateur.service
    $UtilisateurLogin = $UtilisateurPrenom + "." + $UtilisateurNom
    $UtilisateurEmail = "$UtilisateurLogin@$NomDUDomaine"
    $UtilisateurMotDePasse = "Password123"
    $OU_1er_Niveau = Get-ADObject -Filter * -SearchBase "DC=$a,DC=$b"


    # Vérifier la présence de l'utilisateur dans l'AD
    if (Get-ADUser -Filter {SamAccountName -eq $UtilisateurLogin})
    {
        Write-Warning "L'identifiant $UtilisateurLogin existe déjà dans l'AD"
    }
    else
    {



        New-ADUser -Name "$UtilisateurNom $UtilisateurPrenom" `
                   -DisplayName "$UtilisateurNom $UtilisateurPrenom" `
                   -GivenName $UtilisateurPrenom `
                   -Surname $UtilisateurNom `
                   -SamAccountName $UtilisateurLogin `
                   -UserPrincipalName "$UtilisateurLogin@$NomDUDomaine" `
                   -EmailAddress $UtilisateurEmail `
                   -Title $UtilisateurFonction `
                   -AccountPassword(ConvertTo-SecureString $UtilisateurMotDePasse -AsPlainText -Force) `
                   -ChangePasswordAtLogon $false `
                   -Enabled $true `
                   -Path "OU=UTILISATEURS,DC=$a,DC=$b"

sleep 2

try {
Move-ADObject -Identity "CN=$UtilisateurNom $UtilisateurPrenom,OU=UTILISATEURS,DC=$a,DC=$b" -TargetPath "OU=$UtilisateurFonction,OU=$UtilisateurService,OU=$UtilisateurAgence,OU=UTILISATEURS,DC=$a,DC=$b"

}

catch {
Write-Host "Erreur, l'utilisateur $UtilisateurLogin n'a pas été déplacé dans l'OU"
}

finally {


        Write-Output "Création de l'utilisateur : $UtilisateurLogin ($UtilisateurNom $UtilisateurPrenom)"
        }


if ($UtilisateurService -eq "PRODUCTION") {

Add-AdGroupMember -Identity production -Members $UtilisateurLogin
}

if ($UtilisateurService -eq "SECRETAIRE") {

Add-AdGroupMember -Identity secretaire -Members $UtilisateurLogin
}

if ($UtilisateurService -eq "COMMERCIAL") {

Add-AdGroupMember -Identity commercial -Members $UtilisateurLogin
}


    }
}

pause