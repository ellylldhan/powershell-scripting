﻿    If (-NOT ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
{   
$arguments = "& '" + $myinvocation.mycommand.definition + "'"
Start-Process powershell -Verb runAs -ArgumentList $arguments
Break
}



#Creation d'un utilisateur

    $NomDUDomaine = (gwmi WIN32_ComputerSystem).Domain
    $NomDomaineSplit = $NomDUDomaine.Split(".")

    $a = $NomDomaineSplit[0]
    $b = $NomDomaineSplit[1]


    $Prenom = Read-Host "Indiquer le prénom de l'utilisateur"
    $Nom = Read-Host "Indiquer le nom de l'utilisateur"
    $Login = "$Prenom.$Nom"
    $Email = "$Login@$NomDUDomaine"
    $Agence = Read-Host "Indiquer la ville d'agence de l'utilisateur"
    $Service = Read-Host "Indiquer le service de l'utilisateur"
    $Fonction = Read-Host "Indiquer la fonction de l'utilisateur"
    $MotDePasse = Read-Host "Indiquer le mot de passe de l'utilisateur" -AsSecureString

        # Vérifier la présence de l'utilisateur dans l'AD
    if (Get-ADUser -Filter {SamAccountName -eq $Login})
    {
        Write-Warning "L'identifiant $Login existe déjà dans l'AD"
    }
    else
    {



        New-ADUser -Name "$Prenom $Nom" `
                   -DisplayName "$Prenom $Nom" `
                   -GivenName $Prenom `
                   -Surname $Nom `
                   -SamAccountName $Login `
                   -UserPrincipalName "$Login@$NomDUDomaine" `
                   -EmailAddress $Email `
                   -Title $Fonction `
                   -AccountPassword(ConvertTo-SecureString $MotDePasse -AsPlainText -Force) `
                   -ChangePasswordAtLogon $false `
                   -Enabled $true `
                   -Path "OU=UTILISATEURS,OU=TEST,DC=$a,DC=$b" `

}


pause


