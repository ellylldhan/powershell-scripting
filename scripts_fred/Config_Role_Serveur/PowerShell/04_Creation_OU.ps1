﻿    If (-NOT ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
{   
$arguments = "& '" + $myinvocation.mycommand.definition + "'"
Start-Process powershell -Verb runAs -ArgumentList $arguments
Break
}





#Création des OU

$NOM_OU_Base = Read-Host "entrer le nom de l'OU Principale"

$NomDUDomaine = (gwmi WIN32_ComputerSystem).Domain
$NomDomaineSplit = $NomDUDomaine.Split(".")

$a = $NomDomaineSplit[0]
$b = $NomDomaineSplit[1]

New-ADOrganizationalUnit -Name $NOM_OU_Base -Path "DC=$a,DC=$b"
New-ADOrganizationalUnit -Name "UTILISATEURS" -Path "OU=$NOM_OU_Base,DC=$a,DC=$b"
New-ADOrganizationalUnit -Name "ORDINATEURS" -Path "OU=$NOM_OU_Base,DC=$a,DC=$b"
New-ADOrganizationalUnit -Name "IMPRIMANTES" -Path "OU=$NOM_OU_Base,DC=$a,DC=$b"
New-ADOrganizationalUnit -Name "GROUPES" -Path "OU=$NOM_OU_Base,DC=$a,DC=$b"
New-ADOrganizationalUnit -Name "PARTAGES" -Path "OU=$NOM_OU_Base,DC=$a,DC=$b"
New-ADOrganizationalUnit -Name "GG" -Path "OU=GROUPES,OU=$NOM_OU_Base,DC=$a,DC=$b"
New-ADOrganizationalUnit -Name "DL" -Path "OU=GROUPES,OU=$NOM_OU_Base,DC=$a,DC=$b"

Write-Host ""
Write-Host "#### Vérifiaction de l'arborescence créée ####" -ForegroundColor Green


$ErrorActionPreference = 'SilentlyContinue'



$OU_Level1_Name = (Get-ADOrganizationalUnit -filter * -SearchScope onelevel | where Name -NotMatch "Domain*").Name

$OU_Level1_DistinguishedName = (Get-ADOrganizationalUnit -filter * -SearchScope onelevel | where Name -NotMatch "Domain*").DistinguishedName


$OU_Level2_Name = ($OU_Level1_DistinguishedName | Foreach-Object {Get-ADOrganizationalUnit -Filter * -SearchBase $_ -SearchScope OneLevel}).Name

$OU_Level2_DistinguishedName = ($OU_Level1_DistinguishedName | Foreach-Object {Get-ADOrganizationalUnit -Filter * -SearchBase $_ -SearchScope OneLevel}).DistinguishedName


$OU_Level3_Name = ($OU_Level2_DistinguishedName | Foreach-Object {Get-ADOrganizationalUnit -Filter * -SearchBase $_ -SearchScope OneLevel}).Name

$OU_Level3_DistinguishedName = ($OU_Level2_DistinguishedName | Foreach-Object {Get-ADOrganizationalUnit -Filter * -SearchBase $_ -SearchScope OneLevel}).DistinguishedName


$OU_Level4_Name = ($OU_Level3_DistinguishedName | Foreach-Object {Get-ADOrganizationalUnit -Filter * -SearchBase $_ -SearchScope OneLevel}).Name

$OU_Level4_DistinguishedName = ($OU_Level3_DistinguishedName | Foreach-Object {Get-ADOrganizationalUnit -Filter * -SearchBase $_ -SearchScope OneLevel}).DistinguishedName


$OU_Level5_Name = ($OU_Level4_DistinguishedName | Foreach-Object {Get-ADOrganizationalUnit -Filter * -SearchBase $_ -SearchScope OneLevel}).Name

$OU_Level5_DistinguishedName = ($OU_Level4_DistinguishedName | Foreach-Object {Get-ADOrganizationalUnit -Filter * -SearchBase $_ -SearchScope OneLevel}).DistinguishedName


# affichage dans le terminal Powershell

$OU_Level1_Name | Foreach-Object {
write-host "$_" -ForegroundColor Green

$OU_Identity = (Get-ADOrganizationalUnit -Filter * | where name -eq $_).DistinguishedName
$OU2_Name = (Get-ADOrganizationalUnit -Filter * -SearchBase $OU_Identity -SearchScope OneLevel).Name

$OU2_Name | Foreach-Object {
if ($_ -notlike $null){
Write-Host "  ==>$_" -ForegroundColor DarkCyan}

$OU_Identity2 = (Get-ADOrganizationalUnit -Filter * | where name -eq $_).DistinguishedName
$OU3_Name = (Get-ADOrganizationalUnit -Filter * -SearchBase $OU_Identity2 -SearchScope OneLevel).Name

$OU3_Name | Foreach-Object {
if ($_ -notlike $null){
Write-Host "    ==>$_" -ForegroundColor Cyan}

$OU_Identity3 = (Get-ADOrganizationalUnit -Filter * | where name -eq $_).DistinguishedName
$OU4_Name = (Get-ADOrganizationalUnit -Filter * -SearchBase $OU_Identity3 -SearchScope OneLevel).Name

$OU4_Name | Foreach-Object {
if ($_ -notlike $null){
Write-Host "    ==>$_" -ForegroundColor Cyan}

$OU_Identity4 = (Get-ADOrganizationalUnit -Filter * | where name -eq $_).DistinguishedName
$OU5_Name = (Get-ADOrganizationalUnit -Filter * -SearchBase $OU_Identity4 -SearchScope OneLevel).Name

$OU5_Name | Foreach-Object {
if ($_ -notlike $null){
Write-Host "    ==>$_" -ForegroundColor Cyan}

$OU_Identity5 = (Get-ADOrganizationalUnit -Filter * | where name -eq $_).DistinguishedName
$OU6_Name = (Get-ADOrganizationalUnit -Filter * -SearchBase $OU_Identity5 -SearchScope OneLevel).Name

$OU6_Name | Foreach-Object {
if ($_ -notlike $null){
Write-Host "    ==>$_" -ForegroundColor Cyan}

$OU_Identity6 = (Get-ADOrganizationalUnit -Filter * | where name -eq $_).DistinguishedName
$OU7_Name = (Get-ADOrganizationalUnit -Filter * -SearchBase $OU_Identity6 -SearchScope OneLevel).Name

$OU7_Name | Foreach-Object {
if ($_ -notlike $null){
Write-Host "    ==>$_" -ForegroundColor Cyan}

}
}
}
}
}
}
}