﻿#Installation du Rôle DNS et configuration

<#
installation si besoin
#>

  Do{
  
  Write-Host "################ MENU ##################"
  write-host “1. Installer le rôle DNS” -ForegroundColor Cyan
  write-host "2. Créer un redirecteur" -ForegroundColor DarkCyan
  write-host "3. Créer une zone de résolution directe" -ForegroundColor Cyan
  write-host "4. Créer une zone de résolution inverse" -ForegroundColor DarkCyan
  write-host "5. Ajouter un enregistrement de type A & PTR" -ForegroundColor Cyan
  Write-Host "6. Ajouter un enregistrement de type CNAME" -ForegroundColor DarkCyan
  Write-Host "x. Exit" -ForegroundColor Red
  Write-Host "########################################"
  Write-Host ""
  $choix = read-host “faire un choix”

  switch ($choix){  
  

#################################################################################
#1. Installer le rôle DNS

    1{Install-WindowsFeature DNS -IncludeManagementTools
    
    Pause
    
    CLs
    }

#################################################################################
#2. Créer un redirecteur

    2{

# créer un redirecteur conditionnel non lié à l'AD

$Domaine = (gwmi WIN32_ComputerSystem).Domain
$AdresseIP = Read-Host "Indiquer l'adresse IP du redirecteur"

# -replicationScope "Forest" pour créer un redirecteur AD intégré
Add-DnsServerConditionalForwarderZone -Name $Domaine -MasterServers $AdresseIP -PassThru #-ReplicationScope "Forest"

Pause

Cls
}

#################################################################################
#3. Créer une zone de résolution directe

    3{

    $NomZone = Read-Host "Indiquer le nom de la nouvelle zone"
    Write-Host ""

    Add-DnsServerPrimaryZone -Name $NomZone -ReplicationScope "Forest" –PassThru

    Write-Host "###### affichage des zones ######" -ForegroundColor Green
    Write-Host ""

    Get-DnsServerZone   

    Pause

    Cls
    }


#################################################################################
#4. Créer une zone de résolution inverse

    4{

    $IPReseau = Read-Host "Indiquer l'adresse du réseau avec le masque CIDR, ex : 192.168.0.0/24"
    Add-DnsServerPrimaryZone -NetworkId $IPReseau -ReplicationScope Domain

    Pause

    Cls
    }

#################################################################################
#5. Ajouter un enregistrement de type A & PTR

    5{

$NOMHost = Read-Host "Indiquer le NOM de l'hôte"
$IP = Read-Host "Indiquer l'IP de l'hôte"
$NomZone = Read-Host "Indiquer le nom de la zone"
$TTL = Read-Host "Indiquer le TTL, ex : 01:00:00 pour 1h"

Add-DnsServerResourceRecordA -Name $NOMHost -IPv4Address $IP -ZoneName $NomZone -TimeToLive $TTL –CreatePtr

Pause

Cls
}

##################################################################################
#Ajouter un enregistrement de type CNAME

    6{

    $NOMHost = Read-Host "Indiquer le nom d'hôte qui aura l'ALias"
    $Alias = Read-Host "Indiquer l'Alias, sous la forme FQDN"
    $NomZone = Read-Host "Indiquer le nom de la zone"

    Add-DnsServerResourceRecordCName -ZoneName $NomZone -Name $NOMHost -HostNameAlias $Alias

    Pause

    Cls
    }




##################################################################################
#x. exit

    x{exit}


}

}

until ($choix -eq "x")


