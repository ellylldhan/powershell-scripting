﻿    If (-NOT ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
{   
$arguments = "& '" + $myinvocation.mycommand.definition + "'"
Start-Process powershell -Verb runAs -ArgumentList $arguments
Break
}



#Installation du rôle AD DS
Add-WindowsFeature AD-Domain-Services -IncludeManagementTools

#Création d'une nouvelle forêt et d'un nouveau domaine Active Directory, installation du DNS et promouvoir le serveur en contrôleur de domaine
$NomDomaine = Read-Host "entrer le nom de domaine"

$NomDomaineSplit = $NomDomaine.Split(".")
$a = $NomDomaineSplit[0]

Install-ADDSForest -DomainName $NomDomaine -InstallDNS -DomainNetBiosName $a