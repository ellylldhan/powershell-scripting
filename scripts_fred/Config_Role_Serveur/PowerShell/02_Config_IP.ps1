﻿    If (-NOT ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
{   
$arguments = "& '" + $myinvocation.mycommand.definition + "'"
Start-Process powershell -Verb runAs -ArgumentList $arguments
Break
}


  Do{
  
  Write-Host "################ MENU ##################"
  write-host “1. Configuration IP Manuelle” -ForegroundColor Cyan
  write-host "2. Configuration IP en DHCP" -ForegroundColor DarkCyan
  Write-Host "x. Exit" -ForegroundColor Red
  Write-Host "########################################"
  Write-Host ""
  $choix = read-host “faire un choix”

  switch ($choix){



######################################################################################################
# 1. Configuration IP Manuelle

  1{

$Interface = (Get-NetAdapter -physical | where Name -Match "Ether*").Name

$AdresseIP = Read-Host "entrer l'adresse IP"
$MasqueCIDR = Read-Host "entrer le masque de sous-réseau en notation CIDR"
$Passerelle = Read-Host "enrtrer l'IP de la passerelle par défault"
$DNS = Read-Host "entrer l'IP du/des serveurs DNS, ex : 10.53.0.5,8.8.8.8 si plusieurs serveurs DNS"

#suppression IP/masque
Remove-NetIPAddress -InterfaceAlias $Interface -AddressFamily IPv4 -Confirm $false -ErrorAction SilentlyContinue

#Suppression passerelle
Remove-NetRoute -InterfaceAlias $Interface -DestinationPrefix 0.0.0.0/0 -Confirm $false -ErrorAction SilentlyContinue

#Suppression DNS
Set-DnsClientServerAddress -InterfaceAlias $Interface -ResetServerAddresses


New-NetIPAddress -InterfaceAlias $Interface -IPAddress $AdresseIP -PrefixLength $MasqueCIDR -DefaultGateway $Passerelle


#ajouter le/les serveurs DNS
Set-DnsClientServerAddress -InterfaceAlias $Interface -ServerAddresses $DNS

pause

Cls

}


###########################################################################################
# 2. Configuration IP en DHCP

   2{

Get-NetAdapter -Physical
Write-Host ""
$Interface = Read-Host "entrer l'interface à configurer en DHCP"

$Passerelle = (Get-NetRoute -InterfaceAlias $Interface -DestinationPrefix 0.0.0.0/0).NextHop
$PasserelleBool = [Bool] (Get-NetRoute -InterfaceAlias $Interface -DestinationPrefix 0.0.0.0/0).NextHop

if ($PasserelleBool -eq $true){Remove-NetRoute -InterfaceAlias $Interface -NextHop $Passerelle}

Set-NetIPInterface -InterfaceAlias $Interface -Dhcp Enabled

sleep 1

Set-DnsClientServerAddress -InterfaceAlias $Interface -ResetServerAddresses

sleep 1

ipconfig /release

ipconfig /renew

Pause

Cls
}


    x{
    exit
    }


}
}

until ($choix -eq "x")
