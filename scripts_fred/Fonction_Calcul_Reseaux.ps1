﻿<# 

Fonction pour calculer, à partir d'une adresse IP et d'un masque, l'adresse de réseau, l'adresse de broadcast, l'adresse du premier host, l'adresse du dernier host et le nombre d'hotes total.

Utilisation :

Calcul_Reseaux "adresse ip" "masque de sous-réseaux"

exemples :

Calcul_Reseaux "192.168.1.0" "255.255.255.0"

ou avec des variables : Calcul_Reseaux $AdresseIPHost $MasqueHost


#>

function Calcul_Reseaux($AdresseIP,$Masque) {


    # séparation des 4 éléments séparés par les points
$AdresseIPSplit = $AdresseIP.Split(".")

$a = $AdresseIPSplit[0]
$b = $AdresseIPSplit[1]
$c = $AdresseIPSplit[2]
$d = $AdresseIPSplit[3]

# conversion en base 2
$a_binaire = [convert]::ToString($a,2)
$b_binaire = [convert]::ToString($b,2)
$c_binaire = [convert]::ToString($c,2)
$d_binaire = [convert]::ToString($d,2)

#ajout de 0 à gauche avec un maximum de 8 chiffres au total
$a_binaireOctet = $a_binaire.PadLeft(8,'0')
$b_binaireOctet = $b_binaire.PadLeft(8,'0')
$c_binaireOctet = $c_binaire.PadLeft(8,'0')
$d_binaireOctet = $d_binaire.PadLeft(8,'0')

$Conversion_AdresseIP = "$a_binaireOctet.$b_binaireOctet.$c_binaireOctet.$d_binaireOctet"

#---------------------------------------------------------------------------------------

# conversion du masque de sous-réseau en Binaire

$MasqueSplit = $Masque.Split(".")

$e = $MasqueSplit[0]
$f = $MasqueSplit[1]
$g = $MasqueSplit[2]
$h = $MasqueSplit[3]

# conversion en base 2
$e_binaire = [convert]::ToString($e,2)
$f_binaire = [convert]::ToString($f,2)
$g_binaire = [convert]::ToString($g,2)
$h_binaire = [convert]::ToString($h,2)

#ajout de 0 à gauche avec un maximum de 8 chiffres au total
$e_binaireOctet = $e_binaire.PadLeft(8,'0')
$f_binaireOctet = $f_binaire.PadLeft(8,'0')
$g_binaireOctet = $g_binaire.PadLeft(8,'0')
$h_binaireOctet = $h_binaire.PadLeft(8,'0')

$Conversion_Masque = "$e_binaireOctet.$f_binaireOctet.$g_binaireOctet.$h_binaireOctet"

#---------------------------------------------------------------------------------------

# calcul de l'adresse réseau


#séparation de chaque caractère des adresses IP et Masque pour correspondance de chaque Bit ( 1+1=1, 1+0=0 , 0+0=0 )

# partie Adresse IP
$chiffre1 = $Conversion_AdresseIP[0]
$chiffre2 = $Conversion_AdresseIP[1]
$chiffre3 = $Conversion_AdresseIP[2]
$chiffre4 = $Conversion_AdresseIP[3]
$chiffre5 = $Conversion_AdresseIP[4]
$chiffre6 = $Conversion_AdresseIP[5]
$chiffre7 = $Conversion_AdresseIP[6]
$chiffre8 = $Conversion_AdresseIP[7]
$chiffre9 = $Conversion_AdresseIP[9]
$chiffre10 = $Conversion_AdresseIP[10]
$chiffre11 = $Conversion_AdresseIP[11]
$chiffre12 = $Conversion_AdresseIP[12]
$chiffre13 = $Conversion_AdresseIP[13]
$chiffre14 = $Conversion_AdresseIP[14]
$chiffre15 = $Conversion_AdresseIP[15]
$chiffre16 = $Conversion_AdresseIP[16]
$chiffre17 = $Conversion_AdresseIP[18]
$chiffre18 = $Conversion_AdresseIP[19]
$chiffre19 = $Conversion_AdresseIP[20]
$chiffre20 = $Conversion_AdresseIP[21]
$chiffre21 = $Conversion_AdresseIP[22]
$chiffre22 = $Conversion_AdresseIP[23]
$chiffre23 = $Conversion_AdresseIP[24]
$chiffre24 = $Conversion_AdresseIP[25]
$chiffre25 = $Conversion_AdresseIP[27]
$chiffre26 = $Conversion_AdresseIP[28]
$chiffre27 = $Conversion_AdresseIP[29]
$chiffre28 = $Conversion_AdresseIP[30]
$chiffre29 = $Conversion_AdresseIP[31]
$chiffre30 = $Conversion_AdresseIP[32]
$chiffre31 = $Conversion_AdresseIP[33]
$chiffre32 = $Conversion_AdresseIP[34]

# partie Masque
$chiffre1a = $Conversion_Masque[0]
$chiffre2a = $Conversion_Masque[1]
$chiffre3a = $Conversion_Masque[2]
$chiffre4a = $Conversion_Masque[3]
$chiffre5a = $Conversion_Masque[4]
$chiffre6a = $Conversion_Masque[5]
$chiffre7a = $Conversion_Masque[6]
$chiffre8a = $Conversion_Masque[7]
$chiffre9a = $Conversion_Masque[9]
$chiffre10a = $Conversion_Masque[10]
$chiffre11a = $Conversion_Masque[11]
$chiffre12a = $Conversion_Masque[12]
$chiffre13a = $Conversion_Masque[13]
$chiffre14a = $Conversion_Masque[14]
$chiffre15a = $Conversion_Masque[15]
$chiffre16a = $Conversion_Masque[16]
$chiffre17a = $Conversion_Masque[18]
$chiffre18a = $Conversion_Masque[19]
$chiffre19a = $Conversion_Masque[20]
$chiffre20a = $Conversion_Masque[21]
$chiffre21a = $Conversion_Masque[22]
$chiffre22a = $Conversion_Masque[23]
$chiffre23a = $Conversion_Masque[24]
$chiffre24a = $Conversion_Masque[25]
$chiffre25a = $Conversion_Masque[27]
$chiffre26a = $Conversion_Masque[28]
$chiffre27a = $Conversion_Masque[29]
$chiffre28a = $Conversion_Masque[30]
$chiffre29a = $Conversion_Masque[31]
$chiffre30a = $Conversion_Masque[32]
$chiffre31a = $Conversion_Masque[33]
$chiffre32a = $Conversion_Masque[34]


[int] $resultat1 = $chiffre1 + $chiffre1a
[int] $resultat2 = $chiffre2 + $chiffre2a
[int] $resultat3 = $chiffre3 + $chiffre3a
[int] $resultat4 = $chiffre4 + $chiffre4a
[int] $resultat5 = $chiffre5 + $chiffre5a
[int] $resultat6 = $chiffre6 + $chiffre6a
[int] $resultat7 = $chiffre7 + $chiffre7a
[int] $resultat8 = $chiffre8 + $chiffre8a
[int] $resultat9 = $chiffre9 + $chiffre9a
[int] $resultat10 = $chiffre10 + $chiffre10a
[int] $resultat11 = $chiffre11 + $chiffre11a
[int] $resultat12 = $chiffre12 + $chiffre12a
[int] $resultat13 = $chiffre13 + $chiffre13a
[int] $resultat14 = $chiffre14 + $chiffre14a
[int] $resultat15 = $chiffre15 + $chiffre15a
[int] $resultat16 = $chiffre16 + $chiffre16a
[int] $resultat17 = $chiffre17 + $chiffre17a
[int] $resultat18 = $chiffre18 + $chiffre18a
[int] $resultat19 = $chiffre19 + $chiffre19a
[int] $resultat20 = $chiffre20 + $chiffre20a
[int] $resultat21 = $chiffre21 + $chiffre21a
[int] $resultat22 = $chiffre22 + $chiffre22a
[int] $resultat23 = $chiffre23 + $chiffre23a
[int] $resultat24 = $chiffre24 + $chiffre24a
[int] $resultat25 = $chiffre25 + $chiffre25a
[int] $resultat26 = $chiffre26 + $chiffre26a
[int] $resultat27 = $chiffre27 + $chiffre27a
[int] $resultat28 = $chiffre28 + $chiffre28a
[int] $resultat29 = $chiffre29 + $chiffre29a
[int] $resultat30 = $chiffre30 + $chiffre30a
[int] $resultat31 = $chiffre31 + $chiffre31a
[int] $resultat32 = $chiffre32 + $chiffre32a
[int] $resultat33 = $chiffre33 + $chiffre33a
[int] $resultat34 = $chiffre34 + $chiffre34a

# les résultats ci-dessus donne soit 00(0) soit 01(1) soit 10 soit 11, pour avoir l'adresse du réseau : 0 et 0 donne 0, 1 et 0 donne 0 et 1 et 1 donne 1.
if ($resultat1 -eq 0 -or $resultat1 -eq 1 -or $resultat1 -eq 10)

{
$resultatBinaire1 = 0
}

if ($resultat1 -eq 11)

{
$resultatBinaire1 = 1
}

if ($resultat2 -eq 0 -or $resultat2 -eq 1 -or $resultat2 -eq 10)

{
$resultatBinaire2 = 0
}

if ($resultat2 -eq 11)

{
$resultatBinaire2 = 1
}

if ($resultat3 -eq 0 -or $resultat3 -eq 1 -or $resultat3 -eq 10)

{
$resultatBinaire3 = 0
}

if ($resultat3 -eq 11)

{
$resultatBinaire3 = 1
}

if ($resultat4 -eq 0 -or $resultat4 -eq 1 -or $resultat4 -eq 10)

{
$resultatBinaire4 = 0
}

if ($resultat4 -eq 11)

{
$resultatBinaire4 = 1
}

if ($resultat5 -eq 0 -or $resultat5 -eq 1 -or $resultat5 -eq 10)

{
$resultatBinaire5 = 0
}

if ($resultat5 -eq 11)

{
$resultatBinaire5 = 1
}

if ($resultat6 -eq 0 -or $resultat6 -eq 1 -or $resultat6 -eq 10)

{
$resultatBinaire6 = 0
}

if ($resultat6 -eq 11)

{
$resultatBinaire6 = 1
}

if ($resultat7 -eq 0 -or $resultat7 -eq 1 -or $resultat7 -eq 10)

{
$resultatBinaire7 = 0
}

if ($resultat7 -eq 11)

{
$resultatBinaire7 = 1
}

if ($resultat8 -eq 0 -or $resultat8 -eq 1 -or $resultat8 -eq 10)

{
$resultatBinaire8 = 0
}

if ($resultat8 -eq 11)

{
$resultatBinaire8 = 1
}

if ($resultat9 -eq 0 -or $resultat9 -eq 1 -or $resultat9 -eq 10)

{
$resultatBinaire9 = 0
}

if ($resultat9 -eq 11)

{
$resultatBinaire9 = 1
}

if ($resultat10 -eq 0 -or $resultat10 -eq 1 -or $resultat10 -eq 10)

{
$resultatBinaire10 = 0
}

if ($resultat10 -eq 11)

{
$resultatBinaire10 = 1
}

if ($resultat11 -eq 0 -or $resultat11 -eq 1 -or $resultat11 -eq 10)

{
$resultatBinaire11 = 0
}

if ($resultat11 -eq 11)

{
$resultatBinaire11 = 1
}

if ($resultat12 -eq 0 -or $resultat12 -eq 1 -or $resultat12 -eq 10)

{
$resultatBinaire12 = 0
}

if ($resultat12 -eq 11)

{
$resultatBinaire12 = 1
}

if ($resultat13 -eq 0 -or $resultat13 -eq 1 -or $resultat13 -eq 10)

{
$resultatBinaire13 = 0
}

if ($resultat13 -eq 11)

{
$resultatBinaire13 = 1
}

if ($resultat14 -eq 0 -or $resultat14 -eq 1 -or $resultat14 -eq 10)

{
$resultatBinaire14 = 0
}

if ($resultat14 -eq 11)

{
$resultatBinaire14 = 1
}

if ($resultat15 -eq 0 -or $resultat15 -eq 1 -or $resultat15 -eq 10)

{
$resultatBinaire15 = 0
}

if ($resultat15 -eq 11)

{
$resultatBinaire15 = 1
}

if ($resultat16 -eq 0 -or $resultat16 -eq 1 -or $resultat16 -eq 10)

{
$resultatBinaire16 = 0
}

if ($resultat16 -eq 11)

{
$resultatBinaire16 = 1
}

if ($resultat17 -eq 0 -or $resultat17 -eq 1 -or $resultat17 -eq 10)

{
$resultatBinaire17 = 0
}

if ($resultat17 -eq 11)

{
$resultatBinaire17 = 1
}

if ($resultat18 -eq 0 -or $resultat18 -eq 1 -or $resultat18 -eq 10)

{
$resultatBinaire18 = 0
}

if ($resultat18 -eq 11)

{
$resultatBinaire18 = 1
}

if ($resultat19 -eq 0 -or $resultat19 -eq 1 -or $resultat19 -eq 10)

{
$resultatBinaire19 = 0
}

if ($resultat19 -eq 11)

{
$resultatBinaire19 = 1
}

if ($resultat20 -eq 0 -or $resultat20 -eq 1 -or $resultat20 -eq 10)

{
$resultatBinaire20 = 0
}

if ($resultat20 -eq 11)

{
$resultatBinaire20 = 1
}

if ($resultat21 -eq 0 -or $resultat21 -eq 1 -or $resultat21 -eq 10)

{
$resultatBinaire21 = 0
}

if ($resultat21 -eq 11)

{
$resultatBinaire21 = 1
}

if ($resultat22 -eq 0 -or $resultat22 -eq 1 -or $resultat22 -eq 10)

{
$resultatBinaire22 = 0
}

if ($resultat22 -eq 11)

{
$resultatBinaire22 = 1
}

if ($resultat23 -eq 0 -or $resultat23 -eq 1 -or $resultat23 -eq 10)

{
$resultatBinaire23 = 0
}

if ($resultat23 -eq 11)

{
$resultatBinaire23 = 1
}

if ($resultat24 -eq 0 -or $resultat24 -eq 1 -or $resultat24 -eq 10)

{
$resultatBinaire24 = 0
}

if ($resultat24 -eq 11)

{
$resultatBinaire24 = 1
}

if ($resultat25 -eq 0 -or $resultat25 -eq 1 -or $resultat25 -eq 10)

{
$resultatBinaire25 = 0
}

if ($resultat25 -eq 11)

{
$resultatBinaire25 = 1
}

if ($resultat26 -eq 0 -or $resultat26 -eq 1 -or $resultat26 -eq 10)

{
$resultatBinaire26 = 0
}

if ($resultat26 -eq 11)

{
$resultatBinaire26 = 1
}

if ($resultat27 -eq 0 -or $resultat27 -eq 1 -or $resultat27 -eq 10)

{
$resultatBinaire27 = 0
}

if ($resultat27 -eq 11)

{
$resultatBinaire27 = 1
}

if ($resultat28 -eq 0 -or $resultat28 -eq 1 -or $resultat28 -eq 10)

{
$resultatBinaire28 = 0
}

if ($resultat28 -eq 11)

{
$resultatBinaire28 = 1
}

if ($resultat29 -eq 0 -or $resultat29 -eq 1 -or $resultat29 -eq 10)

{
$resultatBinaire29 = 0
}

if ($resultat29 -eq 11)

{
$resultatBinaire29 = 1
}

if ($resultat30 -eq 0 -or $resultat30 -eq 1 -or $resultat30 -eq 10)

{
$resultatBinaire30 = 0
}

if ($resultat30 -eq 11)

{
$resultatBinaire30 = 1
}

if ($resultat31 -eq 0 -or $resultat31 -eq 1 -or $resultat31 -eq 10)

{
$resultatBinaire31 = 0
}

if ($resultat31 -eq 11)

{
$resultatBinaire31 = 1
}

if ($resultat32 -eq 0 -or $resultat32 -eq 1 -or $resultat32 -eq 10)

{
$resultatBinaire32 = 0
}

if ($resultat32 -eq 11)

{
$resultatBinaire32 = 1
}


#--------------------------------------------------------------------------------
# calcul de l'adresse du réseau

$calcul_adresse_reseau_Binaire = "$resultatBinaire1$resultatBinaire2$resultatBinaire3$resultatBinaire4$resultatBinaire5$resultatBinaire6$resultatBinaire7$resultatBinaire8.$resultatBinaire9$resultatBinaire10$resultatBinaire11$resultatBinaire12$resultatBinaire13$resultatBinaire14$resultatBinaire15$resultatBinaire16.$resultatBinaire17$resultatBinaire18$resultatBinaire19$resultatBinaire20$resultatBinaire21$resultatBinaire22$resultatBinaire23$resultatBinaire24.$resultatBinaire25$resultatBinaire26$resultatBinaire27$resultatBinaire28$resultatBinaire29$resultatBinaire30$resultatBinaire31$resultatBinaire32"


$calcul_adresse_reseau_BinaireSplit = $calcul_adresse_reseau_Binaire.Split(".")

$g = $calcul_adresse_reseau_BinaireSplit[0]
$h = $calcul_adresse_reseau_BinaireSplit[1]
$i = $calcul_adresse_reseau_BinaireSplit[2]
$j = $calcul_adresse_reseau_BinaireSplit[3]

# conversion en décimal
$g_Decimal = [convert]::ToInt32($g,2)
$h_Decimal = [convert]::ToInt32($h,2)
$i_Decimal = [convert]::ToInt32($i,2)
$j_Decimal = [convert]::ToInt32($j,2)

$AdresseIPReseau = "$g_Decimal.$h_Decimal.$i_Decimal.$j_Decimal"

# -------------------------------------------------------------------------
#Calcul du premier Host 

$PremierHostBinaire = "$resultatBinaire1$resultatBinaire2$resultatBinaire3$resultatBinaire4$resultatBinaire5$resultatBinaire6$resultatBinaire7$resultatBinaire8.$resultatBinaire9$resultatBinaire10$resultatBinaire11$resultatBinaire12$resultatBinaire13$resultatBinaire14$resultatBinaire15$resultatBinaire16.$resultatBinaire17$resultatBinaire18$resultatBinaire19$resultatBinaire20$resultatBinaire21$resultatBinaire22$resultatBinaire23$resultatBinaire24.$resultatBinaire25$resultatBinaire26$resultatBinaire27$resultatBinaire28$resultatBinaire29$resultatBinaire30$resultatBinaire31" + "1"

$PremierHostBinaireSplit = $PremierHostBinaire.Split(".")

$k = $PremierHostBinaireSplit[0]
$l = $PremierHostBinaireSplit[1]
$m = $PremierHostBinaireSplit[2]
$n = $PremierHostBinaireSplit[3]

# conversion en décimal
$k_Decimal = [convert]::ToInt32($k,2)
$l_Decimal = [convert]::ToInt32($l,2)
$m_Decimal = [convert]::ToInt32($m,2)
$n_Decimal = [convert]::ToInt32($n,2)


$AdresseIPPremierHost = "$k_Decimal.$l_Decimal.$m_Decimal.$n_Decimal"
#-------------------------------------------------------------------------------
# calcul de l'adresse de Broadcast

#Masque binaire sans les points
$compte0 = $Conversion_Masque -replace ("\.")

#calcul de l'index du dernier 1
$index1 = $Compte0.lastindexofany("1")

#avoir le nombre de bit à changer de 0 à 1
$nombre0 = 31 - $index1

#adresse de réseau binaire sans les points
$compte1 = $calcul_adresse_reseau_Binaire -replace ("\.")

#partie adresse broadcast a ne pas modifier
$Broadcast1_sans_point = $compte1.substring(0,$compte1.length -$nombre0)

#adresse de broadcast entiere sans les points ( ajout de 1 jusqu'a 32 caractères)
$Broadcast2_sans_point = $Broadcast1_sans_point.PadRight(32,'1')

# rajout des points à l'adresse de broadcast
$BroadcastBinaire = $Broadcast2_sans_point.ToString().Substring(0, 8) + "." + $Broadcast2_sans_point.ToString().Substring(8, 8) + "." + $Broadcast2_sans_point.ToString().Substring(16, 8) + "." + $Broadcast2_sans_point.ToString().Substring(24, 8) 

# conversion de l'adresse de broadcast en adresse IP

$BroadcastBinaireSplit = $BroadcastBinaire.Split(".")

$o = $BroadcastBinaireSplit[0]
$p = $BroadcastBinaireSplit[1]
$q = $BroadcastBinaireSplit[2]
$r = $BroadcastBinaireSplit[3]

# conversion en décimal
$o_Decimal = [convert]::ToInt32($o,2)
$p_Decimal = [convert]::ToInt32($p,2)
$q_Decimal = [convert]::ToInt32($q,2)
$r_Decimal = [convert]::ToInt32($r,2)

$AdresseIP_Broadcast = "$o_Decimal.$p_Decimal.$q_Decimal.$r_Decimal"
#--------------------------------------------------------------------------------
#Adresse du dernier host

$dernier_Host_Binaire = $BroadcastBinaire -replace (".$","0")

$dernier_Host_BinaireSplit = $dernier_Host_Binaire.Split(".")

$s = $dernier_Host_BinaireSplit[0]
$t = $dernier_Host_BinaireSplit[1]
$u = $dernier_Host_BinaireSplit[2]
$v = $dernier_Host_BinaireSplit[3]

# conversion en décimal
$s_Decimal = [convert]::ToInt32($s,2)
$t_Decimal = [convert]::ToInt32($t,2)
$u_Decimal = [convert]::ToInt32($u,2)
$v_Decimal = [convert]::ToInt32($v,2)

$AdresseIP_dernier_host = "$s_Decimal.$t_Decimal.$u_Decimal.$v_Decimal"

#--------------------------------------------------------------------------------------------
#Calcul du nombre d'hôtes
$NombreHote = ([math]::Pow(2,$nombre0)) - 2

#---------------------------------------------------------------------------------------------
#affichage dans le terminal


Write-Host "Adresse IP du Réseau               : $AdresseIPReseau" -ForegroundColor Magenta

write-host ""

Write-Host "Adresse IP du premier host         : $AdresseIPPremierHost" -ForegroundColor Blue

Write-Host ""

Write-Host "Adresse IP du dernier host         : $AdresseIP_dernier_host" -ForegroundColor Blue

Write-Host ""

write-host "Adresse IP de Broadcast            : $AdresseIP_Broadcast" -ForegroundColor Magenta

write-host ""
Write-Host "Hôtes Total du réseau              : $NombreHote" -ForegroundColor Cyan

write-host ""

}


